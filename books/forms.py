__author__ = 'ivan'

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from .models import Book, Comment


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        exclude = ('author',)
        widgets = {
            'description': forms.Textarea(attrs={'class': 'long-text'}),
        }


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        exclude = ('book', 'user')
        widgets = {
            'text': forms.Textarea(attrs={'class': 'long-text'}),
        }


class UserCreateForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user