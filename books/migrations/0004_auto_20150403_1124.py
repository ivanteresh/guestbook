# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0003_comment_published'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='premoderation',
            field=models.BooleanField(default=False, verbose_name='\u0412\u043a\u043b\u044e\u0447\u0438\u0442\u044c \u043f\u0440\u0435\u043c\u043e\u0434\u0435\u0440\u0430\u0446\u0438\u044e'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='book',
            name='slug',
            field=models.CharField(unique=True, max_length=255, verbose_name='\u0423\u0440\u043b'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='comment',
            name='datetime',
            field=models.DateTimeField(auto_now_add=True),
            preserve_default=True,
        ),
    ]
