# coding: utf-8
from django.db import models
from django.contrib.auth.models import User


class Book(models.Model):
    name = models.CharField(max_length=255, verbose_name=u'Наименование')
    description = models.TextField(max_length=1024, verbose_name="Описание")
    slug = models.CharField(max_length=255, verbose_name=u'Урл', unique=True)
    author = models.ForeignKey(to=User, verbose_name=u'Автор')
    premoderation = models.BooleanField(default=False, verbose_name=u'Включить премодерацию')

    def __unicode__(self):
        return self.name


class Comment(models.Model):
    id = models.AutoField(primary_key=True, verbose_name=u'ID')
    book = models.ForeignKey(to=Book, null=False, blank=False, verbose_name=u'Книга')
    user = models.ForeignKey(to=User, null=True, blank=True, verbose_name='Автор')
    text = models.TextField(max_length=4096, verbose_name="Текст сообщения")
    datetime = models.DateTimeField(auto_now_add=True)
    published = models.BooleanField(default=True, verbose_name=u'Опубликован')

    def __unicode__(self):
        return str(self.user) + ' at ' + str(self.datetime)
