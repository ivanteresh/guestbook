__author__ = 'ivan'

from django.conf.urls import patterns, url
from books.views import *

urlpatterns = patterns(
    '',
    url(r'^create/?', BookCreateView.as_view(), name='create_book'),
    url(r'^moderate/(?P<comment_id>[\d]+)/(?P<action>[-_\w]+)/?$', BookView.moderate, name='moderate'),
    url(r'^(?P<slug>[-_\w]+)/?$', BookView.as_view()),
    )