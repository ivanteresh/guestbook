# coding: utf-8
from django.shortcuts import render, render_to_response, redirect
from django.views.generic import TemplateView
from .forms import BookForm, CommentForm, UserCreateForm
from .models import Book, Comment


class HomeView(TemplateView):
    template_name = 'home.html'

    def get(self, request, *args, **kwargs):

        if not request.user.is_authenticated():
            return redirect('login')

        books = Book.objects.filter(author=request.user)

        comments = Comment.objects.filter(book__in=books).order_by('-datetime')

        context = {'books': books, 'comments': comments, 'moderate': True}

        return self.render_to_response(context)


class RegisterView(TemplateView):
    template_name = 'register.html'

    def get(self, request, *args, **kwargs):
        form = UserCreateForm()

        context = self.get_context_data(**kwargs)
        context.update(
            form=form
        )

        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):

        form = UserCreateForm(request.POST)

        if not form.is_valid() or not form.clean_password2():

            context = self.get_context_data(**kwargs)
            context.update(
                form=form
            )
            return self.render_to_response(context)
        else:
            form.save()

        return redirect('/login')


class BookCreateView(TemplateView):
    template_name = 'create_book.html'

    def get(self, request, *args, **kwargs):
        form = BookForm()

        context = self.get_context_data(**kwargs)
        context.update(
            form=form
        )

        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):

        form = BookForm(request.POST)
        form.instance.author = request.user
        form.save()

        return redirect('/book/'+form.instance.slug)


class BookView(TemplateView):
    template_name = 'book.html'

    def get(self, request, *args, **kwargs):

        context = self.get_context_data(**kwargs)

        book = Book.objects.get(slug=kwargs.get('slug'))
        comments = Comment.objects.filter(book=book)
        comment_form = CommentForm()

        if book.author == request.user:
            moderate = True
        else:
            moderate = False
            comments = comments.filter(published=True)

        context.update(
            book=book,
            comments=comments,
            comment_form=comment_form,
            moderate=moderate
        )

        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):

        form = CommentForm(request.POST)
        book = Book.objects.get(slug=kwargs.get('slug'))

        form.instance.book = book

        if request.user.is_authenticated():
            form.instance.user = request.user

        if book.premoderation:
            form.instance.published = False

        form.save()

        return self.get(request, *args, **kwargs)

    @staticmethod
    def moderate(request, *args, **kwargs):

        referer = request.environ['HTTP_REFERER']

        comm_id = kwargs.get('comment_id')
        action = kwargs.get('action')

        comment = Comment.objects.get(pk=comm_id)

        if action == 'publish':
            comment.published = True
            comment.save()
        elif action == 'delete':
            comment.delete()
        else:
            pass

        return redirect(referer)


