from django.conf.urls import patterns, include, url
from django.contrib import admin
from books.views import *

urlpatterns = patterns('',
    url(r'^$', HomeView.as_view(), name='home'),

    url(r'^register/', RegisterView.as_view(), name='register'),

    url(r'^admin/', include(admin.site.urls)),

    url('^login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}, name='login'),
    url('^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/login'}, name='logout'),

    url(r'^book/', include('books.urls')),

)
